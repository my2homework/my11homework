﻿#include <iostream>
#include <iomanip>
#include <string>

int main()
{
    std::cout << "Enter text: ";
    std::string text;
    std::getline(std::cin, text);

    std::cout << "Your text: " << text << "\n";
    std::cout << "Length: " << text.length() << "\n";
    std::cout << "First letter " << text[0]  << "\n";
    std::cout << "Last letter " << text[text.length() - 1] << "\n";

}
